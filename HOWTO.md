# Server Status Monitor Setup Guide

This comprehensive guide will walk you through the process of setting up and utilizing the Server Status Monitor script in PHP, designed to monitor server status efficiently through daemon commands.

## Prerequisites

Prepare your environment by ensuring the following prerequisites are met:

- PHP installed on your server.
- Composer installed for managing dependencies.
- MySQL server installed and running.

## Installation

1. Clone the repository:

   Use Composer to install the necessary dependencies:

   ```bash
   git clone https://gitlab.com/khushbu2282234/server-status-monitor.git
   cd server-status-monitor
   ```

2. Install dependencies using Composer:

   ```bash
   composer install
   composer require symfony/process
   ```

3. Database table samples:

   Examine the database.sql file for sample database table structures. Create the database and execute the provided queries.

4. Configure the database connection:

   Edit the `config.ini` file and set the appropriate values for your MySQL database:

   ```ini
   [database]
   dsn = "mysql:host=localhost;dbname=[database_name];charset=utf8mb4"
   username = "[database_username]"
   password = "[database_password]"
   ```

5. Run the script:

   Execute the following command to initiate the script:

   ```bash
   php update.php
   ```

## Usage

The script provides various command-line options for customization:

- `-d`: Daemonize the monitoring process (run in the background).
- `-i`: Set the interval between status checks (default is 5 seconds).
- `-n`: Set the number of instances to run simultaneously.
- `-w`: Set the maximum CPU load allowed for processing servers.

Example usage:

```bash
php update.php -d -i 5 -n 2 -w 90
```

Congratulations! Your server status monitoring system is now up and running.
