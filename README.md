# Server Status Monitor

## About Application

The Server Status Monitor is a PHP-based command-line program that allows you to efficiently monitor and store performance statistics of web servers in a MySQL database. This tool is designed to provide real-time insights into your server's health and performance metrics.

## Documentation

For detailed instructions on installing, configuring, and using the Server Status Monitor, refer to the HOWTO.md file in this repository.
