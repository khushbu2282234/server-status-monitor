-- Server table to list all server details
CREATE TABLE `server` (
  `server_id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `httpd` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `port` int(11) unsigned NOT NULL,
  PRIMARY KEY (`server_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- Sample data for server table
INSERT INTO `server` (`name`, `httpd`, `address`, `port`)
VALUES
	('Apache', 'apache', 'apache.org', 80),
	('Yellow Pages', 'apache', 'yellow.com', 80);

-- server status table to store server health details 
CREATE TABLE `server_status` (
  `time` int(11) unsigned NOT NULL,
  `server_id` int(11) unsigned NOT NULL,
  `total_requests` int(11) unsigned NOT NULL,
  `total_kbytes` int(11) unsigned NOT NULL,
  `active_connections` int(11) unsigned NOT NULL,
  KEY `server_id` (`server_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
