<?php
require_once 'vendor/autoload.php'; // Composer autoloader
use Symfony\Component\Process\Process; // Import Process class for daemonization

class WebServerStatusMonitor
{
    private $db;
    private $config;

    public function __construct()
    {
        $this->config = parse_ini_file('config.ini', true); // fetch config file to get database connection details
        $this->connectToDatabase();
        $this->daemonize = false;
        $this->interval = 5;
        $this->instanceCount = 2;
        $this->maxCpuLoad = 80;
    }

    /**
     * Establish a connection to the database
     */
    private function connectToDatabase()
    {
        try {
            $this->db = new PDO($this->config['database']['dsn'], $this->config['database']['username'], $this->config['database']['password']);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die("Database connection failed: " . $e->getMessage());
        }
    }

    /**
     * Retrieve server details and update server status details on database
     */
    public function updateStatus()
    {
        $servers = $this->getServers();
        $this->processServers($servers);
    }

    /**
     * Retrieve server details from the database
     */
    private function getServers()
    {
        $stmt = $this->db->query("SELECT * FROM server");
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Fetch servers from server table
     * Process all server with deamon and get server usage details
     * Store Server details on database 
     */
    private function processServers($servers)
    {
        // Get command-line options
        $options = getopt("di:n:w:");
        $daemonize = $this->daemonize;
        $interval = $this->interval;
        $instanceCount = $this->instanceCount;
        $maxCpuLoad = $this->maxCpuLoad;

        if (isset($options['d'])) {
            $daemonize = true;
        }

        if (isset($options['i'])) {
            $interval = (int) $options['i'];
        }

        if (isset($options['n'])) {
            $instanceCount = (int) $options['n'];
        }

        if (isset($options['w'])) {
            $maxCpuLoad = (int) $options['w'];
        }

        do {
            // Check CPU load
            $cpuLoad = sys_getloadavg()[0];
            if ($cpuLoad < $maxCpuLoad) {
                // Create a new process for each instance
                for ($i = 0; $i < $instanceCount; $i++) {

                    // Process each server
                    foreach ($servers as $server) {

                        // Prepare url based on server list
                        $url = "http://{$server['address']}:{$server['port']}/server-status?auto";

                        // Create a new process using cURL to fetch server status
                        $process = new Process(['curl', '-i', '-s', $url]);
                        $process->run();

                        //If process is not success throw an error with needful information
                        if (!$process->isSuccessful()) {
                            $errorOutput = $process->getErrorOutput();
                            echo "Error: $errorOutput\n";
                            throw new \RuntimeException($process->getErrorOutput());
                        } else {
                            $result = $process->getOutput();

                            // Regular expression to extract the status code
                            if (preg_match('/HTTP\/\d\.\d\s+(\d+)/', $result, $matches)) {
                                $statusCode = (int) $matches[1];

                                if ($statusCode === 200) {

                                    echo " Server: {$server['address']} , HTTP Status Code: $statusCode ";

                                    // Extract data from the status page 
                                    preg_match('/Total Accesses: (\d+)/', $result, $totalAccesses);
                                    preg_match('/Total kBytes: (\d+)/', $result, $totalKBytes);
                                    preg_match('/BusyWorkers: (\d+)/', $result, $busyWorkers);

                                    $time = time();

                                    // Store the data in the database
                                    $pdo = new PDO($this->config['database']['dsn'], $this->config['database']['username'], $this->config['database']['password']);
                                    $stmt = $pdo->prepare('INSERT INTO server_status (time, server_id, total_requests, total_kbytes, active_connections) VALUES (?, ?, ?, ?, ?)');
                                    $stmt->execute([$time, $server['server_id'], $totalAccesses[1], $totalKBytes[1], $busyWorkers[1]]);
                                    $affectedRows = $stmt->rowCount();
                                    echo " Number of affected rows: " . $affectedRows." \n ";
                                } else {
                                    echo " Unexpected HTTP Status Code: $statusCode\n";
                                }
                            } else {
                                echo " Unable to extract HTTP status code from headers.\n";
                            }
                        }
                    }
                }
            }

            sleep($interval);
        } while ($daemonize);
    }
}

// Create an instance of WebServerStatusMonitor and update server status
$monitor = new WebServerStatusMonitor();
$monitor->updateStatus();
